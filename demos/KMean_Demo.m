%%DEMO of kmeans algorithm
FILE=load('clu_data.mat');
data=FILE.data_noise;


disp('K-means');
disp('   ');
disp('Start Processing....');
data

size(data(:,1));
kth_centre_store=[];
square_error_store=[];


ind_info=[];
kth_centre_best=[];
owner_best=[];
square_error_best=[];
for i=1:6
   kth_centre_info=[];
   owner_info=[];
   square_error_info=[];
   
   for iter=1:7
    initialcentres=[];
    for j=1:i
        
        x = floor(1 + (1000-1) * rand);
        initialcentres=[initialcentres;[data(x,1), data(x,2)]];
        
        
    end%end of j
    
    
    [kth_centre,owner,square_error] = kmeans(data, initialcentres);

    kth_centre_info=[kth_centre_info;kth_centre];
    owner_info=[owner_info;owner'];
    square_error_info=[square_error_info;square_error];
    
    
   end%end of iter
   
   min_square_error=min(square_error_info);%getting the minimum squre error for plotting purposes
   ind = find(square_error_info==min_square_error);
   
   square_error_best=[square_error_best;square_error_info(ind(1,:),:)];
   owner_best=[owner_best;owner_info(ind(1,:),:)];
   kth_centre_best=[kth_centre_best;kth_centre_info(ind(1,:),:)];
   

end%end of i 

k=[1,2,3,4,5,6];

figure;

plot(k, square_error_best);
xlabel('K');
ylabel('Sqrt error');
title('sqrt error VS K');
  
figure;
   
    indiv_owner=owner_best(6,:);
    colr= colormap(lines);
    
    for  m = 1:6,
        index = find(indiv_owner==m);
        plot(data(index,1), data(index,2),'.','markersize',10,'markeredgecolor',colr(m,:));hold on;
        if j==1, hold, end;        
    end
    if m ~= 6,figure,end;
    
    

disp('End Processing');
