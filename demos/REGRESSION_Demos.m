%%DEMO of REGRESSION Batch Descent and Gradient Descent 
%(it uses regression_data_x.dat and regression_data_y.dat as data)
X_DATA=load('regression_data_x.dat');
Y_DATA=load('regression_data_y.dat');
  
W=Batch_gradient_descent(X_DATA,Y_DATA,3);
W2=Stochastic_gradient_descent(X_DATA,Y_DATA,3);