%This is a DBScan algorithm for clustering
%input is the minimum number of points in a neighbour and the density in a
%neighbour
%The function returns number of clusters, borders, cores and outliers


function [clust, Border, Core, Outlier]=DBScan(min_pts, Density, data)
%min_pts=5; %min_pts is the number of neighbors in the cluster
%Density=2.5;  % Density is the radius

visited_neighb=zeros(size(data,1),1);
type=ones(size(data,1),1); 
cluster=zeros(size(data,1),1);
 count=0;
    for i=1:size(data,1)
        if visited_neighb(i) 
        continue;
        end
        visited_neighb(i)=1;
        
        [m,n]=size(data);
         Euclid_dist=sqrt(sum((ones(m,1)*data(i,:)-data).^2,2));
         indices=find(Euclid_dist<=Density);
         neighbor_ind=setdiff(indices,i);      
        
        if length(neighbor_ind)<min_pts
            type(i)=-1;
        else
            count=count+1;
            type(i)=1;            
            cluster(i)=count;    
                j=1;
    while j<=length(neighbor_ind)
        if ~visited_neighb(neighbor_ind(j))
            visited_neighb(neighbor_ind(j))=1;
            [m,n]=size(data);
            Euclid_dist=sqrt(sum((ones(m,1)*data(neighbor_ind(j),:)-data).^2,2));
            indices=find(Euclid_dist<=Density);
            secondneighbor_ind=setdiff(indices,neighbor_ind(j));  
            
            if length(secondneighbor_ind)>=min_pts
                neighbor_ind=([neighbor_ind;secondneighbor_ind]);
                type(neighbor_ind(j))=1;
            else                
                type(neighbor_ind(j))=0;
            end
        elseif type(neighbor_ind(j))==-1
            type(neighbor_ind(j))=0;
        end
        if cluster(neighbor_ind(j))==0
            cluster(neighbor_ind(j))=count;
        end
        j=j+1;
    end

                       
            
        end
    end

Border=sum(type==0)
 
Core=sum(type==1)

Outlier=sum(type==-1)
    
clust=(unique(cluster));

figure;
couleurs={ [0 0 1],[0 1 1],[0.75 0.5 0.82],[1 0 0],[0 1 0],[1 0 1],[0 0 0],[0.25 0.9 0.3],[0 0.5 1],[1 0.35 0.2],[0.5 0.9 0.5],[0.45 0.25 0.75],[0.75 0.55 0.75],[0.9 0.7 0.6],[0.9 0.3 0.6],[0.3 0.5 0.9]};

for i=1:size(clust,1)
    spec_clust=find(cluster==clust(i));
    plot(data(spec_clust,1),data(spec_clust,2),'+','Color',couleurs{i});hold on;
    
end

end
 

