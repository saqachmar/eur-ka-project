%This function plots the batch gradient descent regression function of some
%input Y(X) DATA. X_DATA(X_DATA and Y_DATA are column vector)
%it plots also the Error vs number of iterations
%output the coefficient of W of the polynomial

function W=Batch_gradient_descent(X_DATA,Y_DATA, degree_of_polynomial)
disp('Starting the execution:');
plot(X_DATA,Y_DATA,'o');
title('Batch Gradient Descent');
xlabel('X');
ylabel('Y');
m = size(X_DATA,1);%get the size of the data 


X_TEMP=[ones(m,1)]';

for i=1:degree_of_polynomial
    %Here you can approximate by the function you want quadratic,cubic.....
    X_TEMP=[X_TEMP;X_DATA'.^i];

end

X_DATA=X_TEMP;
Y_DATA=Y_DATA';
X_DATA=X_DATA';
W = zeros(size(X_DATA(1,:)))';
X_DATA=X_DATA';
n = size(W,1);      % number of rows
alpha = 0.06;
max_loop=2000;% gradient descent step size
Perror = zeros(max_loop,1);
iterations=zeros(max_loop,1);
for iter = 1 : max_loop 
for i = 1 : n       
    sum = 0;%the sum representing the sigma in the formula in the book(some over all the data points)
    for j = 1 : m
        hW = 0;
        for l = 1 : n
            hW = hW + W(l) * X_DATA(l,j);%sum of hypothesis function which is sigma of w*xi
        end
        sum = sum + ( (hW - Y_DATA(j)) * X_DATA(i,j) ) ;
    end
    W(i) = W(i) - alpha * sum;
end
Z=X_DATA';
error=(1/2).*(((Z*W)-Y_DATA')' * ((Z*W)-Y_DATA'));%calculating the error
Perror(iter) = error;%storing the errors
iterations(iter)=iter;%
end
disp('The final W is:');
W
hold on;
X_DATA=X_DATA';
plot(X_DATA(:,2), X_DATA * W,'-');
hold off;
figure;
plot(iterations,Perror,'-');
title('Error VS Iterations');
xlabel('Iterations');
ylabel('Error');
end