%This function plots the batch stochatic descent regression function of some
%input Y(X) DATA. X_DATA(X_DATA and Y_DATA are column vector)
%it plots also the Error vs number of iterations
%output the coefficient of W of the polynomial

function W=Stochastic_gradient_descent(X_DATA,Y_DATA,degree_of_polynomial)
disp('Starting the execution:');
X_DATA=load('data_x.dat');
Y_DATA=load('data_y.dat');
plot(X_DATA,Y_DATA,'o');
title('Stochastic Gradient Descent');
xlabel('X');
ylabel('Y');
m = size(X_DATA,1); 
X_TEMP=[ones(m,1)]';

for i=1:degree_of_polynomial
    %Here you can approximate by the function you want quadratic,cubic.....
    X_TEMP=[X_TEMP;X_DATA'.^i];

end

X_DATA=X_TEMP;
Y_DATA=Y_DATA';
X_DATA=X_DATA';
W = zeros(size(X_DATA(1,:)))';
X_DATA=X_DATA';
n = size(W,1);      % number of rows
alpha = 0.06;
max_loop=10000;% gradient descent step size

Perror = zeros(max_loop,1);
iterations=zeros(max_loop,1);

for iter = 1 : max_loop;
    for j = 1 : m
        for i = 1 : n 
        hW = 0;
            for l = 1 : n
            hW = hW + W(l) * X_DATA(l,j);
            end
        
        W(i) = W(i) - alpha * (hW - Y_DATA(j))* X_DATA(i,j);
        end
    end

Z=X_DATA';
error=(1/2).*(((Z*W)-Y_DATA')' * ((Z*W)-Y_DATA'));%calculate the error 
Perror(iter) = error;%storing the error
iterations(iter)=iter;%storing the iterations
end
disp('The final W is:');
W
hold on;

X_DATA=X_DATA';
plot(X_DATA(:,2), X_DATA * W,'-');%plot the regression
hold off;
figure;
plot(m.*iterations,Perror,'-');
title('Error VS Data points');
xlabel('Iterations');
ylabel('Error');
end