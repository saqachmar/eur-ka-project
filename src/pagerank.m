%% PageRank Implementation(Power Iteration)
%PageRank is a link analysis algorithm, named after Larry Page and used by
%the Google Internet search engine, that assigns a numerical weighting to 
%each element of a hyperlinked set of documents, such as the World Wide Web,
%with the purpose of "measuring" its relative importance within the set.
%The algorithm may be applied to any collection of entities with reciprocal
%quotations and references [related to webpages]. The numerical weight that
%it assigns to any given element E is referred to as the PageRank of E and
%denoted by PR(E).
% 
%Input : Hyperlink Matrix and the Initialization of the following form 
%      M=[0,1,1,0,0,0;      v=[1;0;0;0;0;0];
%       0,0,0,1/3,0,0;
%       0,0,0,1/3,1/2,0;
%       1,0,0,0,0,1;
%       0,0,0,1/3,0,0;
%       0,0,0,0,1/2,0];
%
%Output : Rk as the ranking vector and eigenvalues of the matrix since it
%is used to check the convergance if it is slow or fast (close to 1 -->
%slow - close to 0 --> fast ) 
%
%%
function [Rk, eigenvalues] = pagerank(M,v)
   
    r(:,1)=v;
    idx=2;
    threshould =0.0001;
    % any initial value and used to check the diffrence of the old vector
    % and the new one according some threshould value.
    dlta=10000;
    while dlta>threshould
        oldx=r(:,idx-1);
        r(:,idx)=M*oldx;
        dlta=norm(r(:,idx)-oldx,1);
        idx=idx+1;
    end
    Rk=r(:,end);
    eigenvalues=eig(M);
end