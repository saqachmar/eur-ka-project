%Kmeans is a clustering algorithm which needs as input initial centers
%which can be taken as random
%The output is the k_th center, clusters and the square error
function [kth_centre,clust,square_error] = kmeans(data, initialcentres)

N = size(data,1); % num data points
L = size(data,2); % num variables

if (max(size(initialcentres))==1)
    K = initialcentres;
    initialcentres = data(1:K,:);
end

if (size(initialcentres,2) ~= L)
    initialcentres = initialcentres';
end


K = size(initialcentres,1); % num clusters


kth_centre = initialcentres; % K * L
clustchange = 1;
clust = zeros(N,1);
while (clustchange > 0)
    oldkth_centre = kth_centre;

%compute squared error for each point
    square_error = 0;
    clustchange = 0;
    for i = 1:N
        % find distance to each cluster centre
        best = 0;
        bestdist = 10^100;
        for j = 1:K
            dist = sum((data(i,:) - kth_centre(j,:)).^2);
            if (dist < bestdist)
                best=j;
                bestdist=dist;
            end
        end
        square_error = square_error + bestdist;

        
        if (clust(i) ~= best)
            clust(i) = best;
            clustchange = 1;
        end
    end


    % Recompute centres for each cluster
    for i = 1:K
        total = sum(clust==i);
        % for each variable
        if (total > 0)
            for j = 1:L
                kth_centre(i,j) = sum((clust==i).*(data(:,j))) / total;
            end
        else
            kth_centre(i,:) = data(floor(rand*N)+1,:)+(10^-10)*randn(1,L);
        end
    end

end