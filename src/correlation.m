%% Implementation of Person's Correlation algorithm (Used in Recommendation
%and finding the similarity) of a data where every record will be
% compared with all other records to find out how it is similar to other 
%records. the result is "CorrMatrixResult" which is a
% matrix of correlation. the second part of the code will read the matrix
% and reformat it to be as the below table format, that provide easier
% readability and usability.
%   |Record A | Record B | Correlation Value|
%
% Input: data file in CSV format where every row considered as one record
%        and each column represent a feature.
% Output: 
%       1- CorrMatrixResult : Correlation matrix of all data
%       2- similarityTable  : Table of the result in more readable format
%%

function [CorrMatrixResult,similarityTable] = correlation(Data)

    TransposedData=Data';
    CorrMatrixResult=corr(TransposedData);

    similarityTable=[0,0,0];

    for i=1 : size(CorrMatrixResult,1)
        for j=1 : size(CorrMatrixResult,1)
            similarityTable=[similarityTable;[i,j,CorrMatrixResult(i,j)*1000]];
        end
    end

end