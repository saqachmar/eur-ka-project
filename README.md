About:
------
"This is an eureka project that gathers Machine learning developped by Ayoub Aneddame and Khalied Al Barrak. 
This contains set of function and a test file representing a demo explaining how the functions can be used by users! 
This project is part of Scientific Software Engineering aiming to contribute to the community"


ALGORITHMS :
------------
	1- Batch gradient descent :is a gradient descent optimization method for 
							   minimizing an objective function that is written
							   as a sum of differentiable functions.
	2- Pearson's Correlation:is a measure of the correlation (linear dependence)
						     between two variables X and Y, giving a value between
							 +1 and ?1 inclusive. It is widely used in the sciences
							 as a measure of the strength of linear dependence
							 between two variables.
	3- DBScan :is a data clustering algorithm. It is a density-based clustering
			   algorithm because it finds a number of clusters starting from the
			   estimated density distribution of corresponding nodes. 
	4- Kmeans :is a method of cluster analysis which aims to partition n
			   observations into k clusters in which each observation belongs
			   to the cluster with the nearest mean. This results in a partitioning
			   of the data space into Voronoi cells.
	5- Pagerank	:is a link analysis algorithm, named after Larry Page and used
				  by the Google Internet search engine, that assigns a numerical
				  weighting to each element of a hyperlinked set of documents, 
				  such as the World Wide Web, with the purpose of "measuring" its
			      relative importance within the set.
	6- Stochastic gradient descent :is a gradient descent optimization method for 
								   minimizing an objective function that is written
								   as a sum of differentiable functions.

Project Folers:
---------------
	1- src	: Contains all the soruce files of the algorithms.
	2- demos: Contains all the demos for testing all the algorithms.
	3- data : Contains all data used by the demos and some other data files.

How to run the Algorithms:
--------------------------
	For running the code, you have to have a Matlab installed in any version and
	under any environment.
	
	How to use Eureka? You can use Eureka easily by adding the "src file" to
	matlab path and that can be done by :
	
	1- in the file navigation section of matlab (usually on the left side), go to
		the directory that you saved Eureka in.
	2- Right click on Eureka Project folder, at the bottom of the popup menu click
	   on "Add to path" then click on "Selected folders and subfolders"
	3- Open new script.
	4- Type your code, and the function name that you need to use "More info about
	   each algorithm can be found in Algorithm .m in the src folder".
	   (You can use the demos)


Demos:
------

	1- REGRESSION_Demos.m : a demonstration for the usage of "Stochastic gradient 
							descent" as well as "Batch gradient descent" algorithms
	2- PageRank_Demo.m    : a demonstration for the usage of "PageRank algorithm"
	3- KMean_Demo.m		  : a demonstration for the usage of "KMean algorithm"
	4- DEBSCAN_Demo.m     : a demonstration for the usage of "DEBSCAN algorithm"
	5- Correlation_Demo.m : a demonstration for the usage of "Correlation algorithm"